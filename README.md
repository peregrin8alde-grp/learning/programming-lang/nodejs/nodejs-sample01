# nodejs-sample01

npm は使わない一番単純なサンプル

- Docker イメージ : https://hub.docker.com/_/node
  - 説明 : https://github.com/nodejs/docker-node/blob/main/README.md#how-to-use-this-image
  - ベストプラクティス : https://github.com/nodejs/docker-node/blob/main/docs/BestPractices.md
- 公式ドキュメント : https://nodejs.org/docs/latest/api/
- 公式の Web サーバサンプル : https://nodejs.org/docs/latest/api/synopsis.html
- 公式のコンソール出力サンプル : https://nodejs.org/docs/latest/api/console.html


コマンドラインの実行

```
docker run \
  -it --rm \
  -u node \
  -v "$PWD/src":/usr/src/app \
  -w /usr/src/app \
  -e "NODE_ENV=development" \
  node:16 \
    node console.js
```

サーバの起動

```
docker run \
  -it --rm \
  --name nodejs-sample01 \
  -u node \
  -p 3000:3000 \
  -v "$PWD/src":/usr/src/app \
  -w /usr/src/app \
  -e "NODE_ENV=development" \
  node:16 \
    node hello-world.js
```

Ctrl_C などでは停止できないため注意

